package com.example.fibonaccinumbers

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.WorkInfo
import androidx.work.WorkManager
import androidx.work.workDataOf
import com.example.fibonaccinumbers.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var workManager: WorkManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        workManager = WorkManager.getInstance(applicationContext)

        binding.Button.setOnClickListener {
            val index = try {
                binding.input.text.toString().toInt()
            } catch (e: java.lang.NumberFormatException) {
                showToast()
                return@setOnClickListener
            }

            if (!isRequestSent()) {
                modyifyUI(requestSent = false)
            } else {
                workManager.cancelAllWork()
                modyifyUI(requestSent = true)
                return@setOnClickListener
            }

            sendRequest(index)
        }
    }

    private fun sendRequest(index: Int) {
        val workRequest = OneTimeWorkRequestBuilder<FibonacciWorker>()
            .setInputData(workDataOf("index" to index))
            .build()
        workManager.enqueue(workRequest)
        workManager.getWorkInfoByIdLiveData(workRequest.id)
            .observe(this, Observer {
                when (it.state) {
                    WorkInfo.State.SUCCEEDED -> {
                        val result = it.outputData.getInt(DATA_KEY, 0)
                        binding.currentNumber.text = getString(R.string.result_is, result)
                        binding.input.isEnabled = true
                        binding.Button.text = getString(R.string.start)
                    }
                    WorkInfo.State.RUNNING -> {
                        val progress = it.progress.getInt(MESSAGE_KEY, 0)
                        binding.currentNumber.text = getString(R.string.current_is_d, progress)
                    }
                    else -> return@Observer
                }
            })
    }

    private fun modyifyUI(requestSent: Boolean) {
        if(!requestSent) {
            binding.Button.text = getString(R.string.cancel)
            binding.currentNumber.visibility = View.VISIBLE
            binding.input.isEnabled = false
        } else {
            binding.Button.text = getString(R.string.start)
            binding.currentNumber.visibility = View.INVISIBLE
            binding.input.isEnabled = true
        }
    }

    private fun isRequestSent(): Boolean {
        return !binding.input.isEnabled
    }

    private fun showToast() {
        val message = "Please enter a valid number"
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }
}