package com.example.fibonaccinumbers

import android.content.Context
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import androidx.work.workDataOf
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.withContext

class FibonacciWorker(appContext: Context, params: WorkerParameters) :
    CoroutineWorker(appContext, params) {
    override suspend fun doWork(): Result {

        val index = inputData.getInt("index", 0)
        val data = computeFibonacci(index)

        val result = workDataOf(DATA_KEY to data)
        return Result.success(result)
    }

    private suspend fun computeFibonacci(index: Int): Int = withContext(Dispatchers.IO) {
        val f = arrayOf(0, 1)
        var tmp: Int
        for (i in 0..index) {
            setProgress(workDataOf(MESSAGE_KEY to i))
            delay(1000)
            if(i <  2) continue
            tmp = f[1]
            f[1] = f[0] + f[1]
            f[0] = tmp
        }
        return@withContext f[1]
    }
}